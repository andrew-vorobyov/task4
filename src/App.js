import React, {Component} from 'react';
import axios from 'axios';

class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            exercises: [],
        }

        this.randerExircises = this.randerExircises.bind(this);
    }

    componentWillMount() {

        let url = 'http://dev.videotherapy.co/vt/api/dispatcher.php';
        let data = {
            "api": "exercises/get-exercises",
            "clientKey": "8e28b8db-6395-4417-9df9-10dd0efb5ef9",
            "paging":
                {
                    "limit": 12,
                    "offset": 0
                }
        };

        const self = this;

        axios.post(url, data)
            .then((response) => {
                    self.setState({
                        exercises: response.data.data,
                    });
                }
            ).catch((error) => console.warn(error));
    }

    randerExircises() {

        if (!this.state.exercises.length) return;

        let exercisesFinal = this.state.exercises.filter((exercise) => {
            return (exercise['BodyZoneJson'].indexOf(1) !== -1);
        });

        return ([
            exercisesFinal.map((exercise) => {
                return (
                    <li key={exercise.id}>
                       <span>{exercise.label}</span>
                    </li>
                )
            })
        ]);
    }

    render() {
        return (
            <div className="App">
                <h3>Exercises final</h3>
                <ol>
                    {this.randerExircises()}
                </ol>
            </div>
        );
    }
}

export default App;
